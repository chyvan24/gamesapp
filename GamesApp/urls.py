"""GamesApp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.contrib.auth.views import LogoutView
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from app.views import IndexView, GameView, \
    RegisterView, LoginView, \
    CreateGameView, DeleteGameView, UpdateGameView, \
    DeleteCharacterView, UpdateCharacterView, CreateCharacterView, \
    CreateTeamView, DeleteTeamView, UpdateTeamView
from app.views.searchView import SearchView

urlpatterns = []

urlpatterns += i18n_patterns(
    path('admin/', admin.site.urls),
    url(r'^$', IndexView.as_view(), name='app_index'),
    url(r'^game/(?P<title>\w+)/$', GameView.as_view(), name='app_game'),

    url(r'^search/$', SearchView.as_view(), name='app_search'),

    url(r'^add/game/$', CreateGameView.as_view(success_url="/"), name='app_add_game'),
    url(r'^delete/game/(?P<pk>[\d]+)$', DeleteGameView.as_view(success_url="/"), name='app_delete_game'),
    url(r'^update/game/(?P<pk>[\d]+)$', UpdateGameView.as_view(success_url="/"), name='app_update_game'),

    url(r'^add/character/$', CreateCharacterView.as_view(success_url="/"), name='app_add_character'),
    url(r'^delete/character/(?P<pk>[\d]+)$', DeleteCharacterView.as_view(success_url="/"), name='app_delete_character'),
    url(r'^update/character/(?P<pk>[\d]+)$', UpdateCharacterView.as_view(success_url="/"), name='app_update_character'),

    url(r'^add/team/$', CreateTeamView.as_view(success_url="/"), name='app_add_team'),
    url(r'^delete/team/(?P<pk>[\d]+)$', DeleteTeamView.as_view(success_url="/"), name='app_delete_team'),
    url(r'^update/team/(?P<pk>[\d]+)$', UpdateTeamView.as_view(success_url="/"), name='app_update_team'),



    url(r'^register/?$', RegisterView.as_view(success_url="/"), name="app_register"),
    url(r'^logout/$', LogoutView.as_view(), {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout'),
    url(r'^login/$', LoginView.as_view(success_url=settings.LOGIN_REDIRECT_URL), name='app_login'),
)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
