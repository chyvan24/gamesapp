from django.contrib import admin

from app.models import Game, Tag, Type, Character, Team

admin.site.register(Game)
admin.site.register(Tag)
admin.site.register(Type)
admin.site.register(Character)
admin.site.register(Team)
