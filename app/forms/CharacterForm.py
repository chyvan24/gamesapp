from django.forms import models, TextInput, Textarea, ImageField, ModelChoiceField

from app.models import Game, Character, Tag


class CharacterForm(models.ModelForm):
    class Meta:
        model = Character
        fields = ['name', 'game', 'description', 'story', 'type', 'tags', 'image']
        widgets = {
            'name': TextInput(attrs={'class': 'form-control', 'placeholder': 'Name'}),
            'description': Textarea(attrs={'class': 'form-control', 'placeholder': 'Description'}),
            'story': Textarea(attrs={'class': 'form-control', 'placeholder': 'Story'}),
        }
