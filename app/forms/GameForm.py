from django.forms import models, TextInput, Textarea, ImageField

from app.models import Game


class GameForm(models.ModelForm):
    class Meta:
        model = Game
        fields = ['title', 'description', 'image']
        widgets = {
            'title': TextInput(attrs={'class': 'form-control', 'placeholder': 'Title'}),
            'description': Textarea(attrs={'class': 'form-control', 'placeholder': 'Description'}),
        }
