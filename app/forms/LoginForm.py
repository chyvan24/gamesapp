from django import forms
from django.forms import widgets


class LoginForm(forms.Form):

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)

    username = forms.CharField(max_length=200, min_length=4, widget=widgets.TextInput(attrs={'class': 'form-control', 'placeholder': 'Username'}))
    password = forms.CharField(max_length=200, min_length=8, widget=widgets.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Password'}))


