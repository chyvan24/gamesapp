from django import forms
from django.forms import widgets


class RegisterForm(forms.Form):

    def __init__(self, *args, **kwargs):
        super(RegisterForm, self).__init__(*args, **kwargs)

    username = forms.CharField(max_length=200, min_length=4, widget=widgets.TextInput(attrs={'class': 'form-control', 'placeholder': 'Username'}))
    email = forms.CharField(max_length=200, min_length=4, widget=widgets.EmailInput(attrs={'class': 'form-control', 'placeholder': 'Email address'}))
    password_1 = forms.CharField(max_length=200, min_length=8, widget=widgets.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Password'}))
    password_2 = forms.CharField(max_length=200, min_length=8, widget=widgets.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Password verification'}))

    def clean_username(self):
        username = self.cleaned_data['username']
        if not username.isalnum():
            self.add_error('username', 'Only alphanum chars allowed !')
            return None
        return username
