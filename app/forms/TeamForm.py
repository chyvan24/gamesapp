from django.forms import models, TextInput, Textarea, ImageField, ModelChoiceField
from django.contrib.auth.models import User

from app.models import Game, Character, Type, Tag, Team


class TeamForm(models.ModelForm):
    class Meta:
        model = Team
        fields = ['title', 'game', 'description', 'characters', 'tags']
        widgets = {
            'title': TextInput(attrs={'class': 'form-control', 'placeholder': 'Title'}),
            'description': Textarea(attrs={'class': 'form-control', 'placeholder': 'Description'}),
            'story': Textarea(attrs={'class': 'form-control', 'placeholder': 'Story'}),
        }
