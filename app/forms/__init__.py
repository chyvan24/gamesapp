from .RegisterForm import RegisterForm
from .LoginForm import LoginForm
from .GameForm import GameForm
from .CharacterForm import CharacterForm
