from django.db import models

from app.models import Tag, Game, Type


class Character(models.Model):
    name = models.CharField(max_length=50, blank=False, null=False)
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    description = models.TextField(max_length=200, blank=True, null=True)
    story = models.TextField(max_length=2000, blank=True, null=True)
    type = models.ForeignKey(Type, on_delete=models.SET_NULL, null=True, blank=True)
    tags = models.ManyToManyField(Tag)
    image = models.ImageField(upload_to='characters/', default='unknown.png')

    def __str__(self):
        return self.name + '(' + self.game.title + ')'
