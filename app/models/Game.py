from django.db import models
from django.forms import widgets


class Game(models.Model):
    title = models.CharField(max_length=50, blank=False, null=False,unique=True)
    description = models.TextField(max_length=200, blank=False, null=False)
    image = models.ImageField(upload_to='games/', default='unknown.png')

    def __str__(self):
        return self.title
