from django.db import models


class Tag(models.Model):
    description = models.TextField(max_length=20, blank=False, null=False,unique=True)

    def __str__(self):
        return self.description
