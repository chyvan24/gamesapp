from django.contrib.auth.models import User
from django.db import models

from app.models import Character, Game, Tag


class Team(models.Model):
    title = models.CharField(max_length=50, blank=False, null=False)
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    description = models.TextField(max_length=200, blank=True, null=True)
    characters = models.ManyToManyField(Character, blank=False)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    tags = models.ManyToManyField(Tag)

    def __str__(self):
        return self.title + '(' + self.game.title + ')'


