from django.views.generic import ListView,FormView
from pyexpat.errors import messages

from app.forms import GameForm
from app.models import Game


class IndexView(ListView):
    template_name = 'index.html'
    model = Game
    success_url = '/'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['games'] = Game.objects.all().order_by('title')
        context['game_form'] = GameForm

        return context
