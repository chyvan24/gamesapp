from django.contrib.auth import authenticate, login
from django.shortcuts import redirect, render

from django.views.generic import FormView

from app.forms import LoginForm


class LoginView(FormView):
    template_name = 'login.html'
    form_class = LoginForm

    def post(self, request, **kwargs):
        form = LoginForm;
        username = request.POST.get('username', False)
        password = request.POST.get('password', False)
        user = authenticate(username=username, password=password)
        if user is not None and user.is_active:
            login(request, user)
            return redirect('app_index')
        return render(request, 'login.html', {'form': form})
