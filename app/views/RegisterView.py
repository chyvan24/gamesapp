from django.contrib.auth.models import User
from django.shortcuts import redirect
from django.views.generic import FormView

from app.forms import RegisterForm


class RegisterView(FormView):
    template_name = 'register.html'
    form_class = RegisterForm

    def form_valid(self, form):
        data = form.cleaned_data
        username = data['username']
        email = data['email']
        password = data['password_1']
        try:
            user = User.objects.create_user(username=username, email=email, password=password)
        except:
            return redirect('app_register')
        return super().form_valid(form)
