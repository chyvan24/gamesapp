from .IndexView import IndexView

from .RegisterView import RegisterView
from .LoginView import LoginView
from .game import GameView, CreateGameView, DeleteGameView, UpdateGameView
from .character import CreateCharacterView, DeleteCharacterView, UpdateCharacterView
from .team import CreateTeamView, DeleteTeamView, UpdateTeamView