from audioop import reverse

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import CreateView

from app.forms import CharacterForm
from app.models import Character


@method_decorator(login_required, name='dispatch')
class CreateCharacterView(CreateView):
    template_name = 'characterviews/addCharacter.html'
    model = Character
    form_class = CharacterForm
