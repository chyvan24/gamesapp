from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic.edit import DeleteView

from app.models import Character


@method_decorator(login_required, name='dispatch')
class DeleteCharacterView(DeleteView):
    template_name = 'characterviews/character_confirm_delete.html'
    model = Character

    def get_object(self, queryset=None):
        Character = super(DeleteCharacterView, self).get_object()
        return Character
