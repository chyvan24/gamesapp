from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import UpdateView

from app.forms import CharacterForm
from app.models import Character


@method_decorator(login_required, name='dispatch')
class UpdateCharacterView(UpdateView):
    template_name = 'characterviews/updateCharacter.html'
    model = Character
    form_class = CharacterForm
