from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import CreateView

from app.forms import GameForm
from app.models import Game


@method_decorator(login_required, name='dispatch')
class CreateGameView(CreateView):
    template_name = 'gameviews/addGame.html'
    model = Game
    form_class = GameForm
