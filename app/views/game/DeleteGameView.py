from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic.edit import DeleteView

from app.models import Game


@method_decorator(login_required, name='dispatch')
class DeleteGameView(DeleteView):
    template_name = 'gameviews/game_confirm_delete.html'
    model = Game

    def get_object(self, queryset=None):
        game = super(DeleteGameView, self).get_object()
        return game
