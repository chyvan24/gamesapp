from django.shortcuts import get_object_or_404
from django.views.generic import ListView

from app.models import Character, Type, Tag, Game, Team


class GameView(ListView):
    template_name = 'gameviews/game.html'

    def get_queryset(self):
        title = self.kwargs.get('title', '')
        self.game = get_object_or_404(Game, title__exact=title)
        return Game.objects.filter(title__exact=title)

    def get_context_data(self, **kwargs):
        context = super(GameView, self).get_context_data(**kwargs)
        context['game'] = self.game
        context['characters'] = Character.objects.filter(game__exact=self.game).order_by('name')
        context['teams'] = Team.objects.filter(game__exact=self.game).order_by('game__title')
        return context

