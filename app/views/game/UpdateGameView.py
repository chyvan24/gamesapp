from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import UpdateView

from app.forms import GameForm
from app.models import Game


@method_decorator(login_required, name='dispatch')
class UpdateGameView(UpdateView):
    template_name = 'gameviews/updateGame.html'
    model = Game
    form_class = GameForm
