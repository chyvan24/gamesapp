from django.contrib.auth.models import User
from django.db.models import Q
from django.shortcuts import get_object_or_404
from django.views.generic import ListView

from app.models import Character, Type, Tag, Game, Team


class SearchView(ListView):
    template_name = 'search.html'

    def get_queryset(self, **kwargs):
        slug = self.request.GET.get('slug',)
        self.slug = slug
        return slug

    def get_context_data(self, **kwargs):
        context = super(SearchView, self).get_context_data(**kwargs)
        slug = self.slug
        context['slug'] = slug
        users = User.objects.filter(Q(username__contains=slug))
        tags = Tag.objects.filter(Q(description__contains=slug))
        types = Type.objects.filter(Q(name__contains=slug))
        games = Game.objects.filter(Q(title__contains=slug))
        context['games'] = games.distinct().order_by('title')
        context['characters'] = Character.objects.filter(Q(game__in=games)
                                                         | Q(name__contains=slug)
                                                         | Q(type__in=types)
                                                         | Q(tags__in=tags)
                                                         ).distinct().order_by('name')
        context['teams'] = Team.objects.filter(Q(game__in=games)
                                               | Q(owner__in=users)
                                               | Q(title__contains=slug)
                                               | Q(tags__in=tags)
                                               ).distinct().order_by('game__title')
        return context
