from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import CreateView

from app.forms.TeamForm import TeamForm
from app.models import Team


@method_decorator(login_required, name='dispatch')
class CreateTeamView(CreateView):
    template_name = 'teamviews/addTeam.html'
    model = Team
    form_class = TeamForm

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super(CreateTeamView, self).form_valid(form)
