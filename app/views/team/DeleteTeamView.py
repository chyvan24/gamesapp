from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.utils.decorators import method_decorator
from django.views.generic.edit import DeleteView

from app.models import Team


@method_decorator(login_required, name='dispatch')
class DeleteTeamView(DeleteView):
    template_name = 'teamviews/Team_confirm_delete.html'
    model = Team

    def get_object(self, queryset=None):
        team = super(DeleteTeamView, self).get_object()
        if (team.owner == self.request.user) or (self.request.user.groups.filter(name='moderator').exists()):
            return team
        raise Http404
