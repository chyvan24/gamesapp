from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import UpdateView

from app.forms.TeamForm import TeamForm
from app.models import Team


@method_decorator(login_required, name='dispatch')
class UpdateTeamView(UpdateView):
    template_name = 'teamviews/updateTeam.html'
    model = Team
    form_class = TeamForm
